-module(curling_feed).
%%% ========================================================= %%%
-behaviour(gen_event).
%%% ========================================================= %%%
-export([init/1]).
-export([handle_call/2]).
-export([handle_event/2]).
-export([handle_info/2]).
-export([terminate/2]).
%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init(Pid) ->
    {ok, Pid}.

handle_call(_Event, Pid) ->
    {ok, ok, Pid}.

handle_event(Event, Pid) ->
    Pid ! {curling_feed, Event},
    {ok, Pid}.

handle_info(_Event, Pid) ->
    {ok, Pid}.

terminate(_Reason, _Pid) ->
    ok.
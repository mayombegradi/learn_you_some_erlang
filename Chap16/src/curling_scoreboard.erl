-module(curling_scoreboard).
%%% ========================================================= %%%
-behaviour(gen_event).
%%% ========================================================= %%%
-export([init/1]).
-export([handle_call/2]).
-export([handle_event/2]).
-export([handle_info/2]).
-export([terminate/2]).
%%% ========================================================= %%%
-define(SCOREBOARD, curling_scoreboard_hw).
%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init(_Args) ->
    {ok, []}.

handle_call(_Event, State) ->
    {ok, ok, State}.

handle_event({set_teams, TeamA, TeamB}, State) ->
    ?SCOREBOARD:set_teams(TeamA, TeamB),
    {ok, State};
handle_event({add_points, Team, N}, State) ->
    [?SCOREBOARD:add_point(Team) || _Seq <- lists:seq(1, N)],
    {ok, State};
handle_event(next_round, State) ->
    ?SCOREBOARD:next_round(),
    {ok, State};
handle_event(_Event, State) ->
    {ok, State}.

handle_info(_Event, State) ->
    {ok, State}.

terminate(_Reason, _State) ->
    ok.
{application, erlcount,
 [{description, "Run regular expressions on Erlang source files"},
  {vsn, "1.0.0"},
  {registered, [erlcount]},
  {mod, {erlcount, []}},
  {applications,
   [kernel,
    stdlib,
    sasl,
    ppool
   ]},
  {env,[{directory, "."},
        {regex, ["if\\s.+->", "case\\s.+\\sof"]},
        {max_files, 10}]},
  {modules, [erlcount, erlcount_sup, erlcount_lib,
             erlcount_dispatch, erlcount_counter]}
 ]}.
-module(erlcount).
%%% ========================================================= %%%
-behaviour(application).
%%% ========================================================= %%%
-export([start/2]).
-export([stop/1]).
%%% ========================================================= %%%
-type start_type() :: normal.
-type args() :: []. %% See ppool.app.src, {mod, {ppool_app, []}}.
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start(start_type(), args()) -> {ok, pid()}.
start(normal, _Args) ->
    erlcount_sup:start_link().

-spec stop(term()) -> no_return().
stop(_State) ->
    ok.
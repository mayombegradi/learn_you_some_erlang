-module(erlcount_dispatch).
%%% ========================================================= %%%
-behaviour(gen_statem).
%%% ========================================================= %%%
-export([start_link/0]).
-export([complete/4]).

-export([init/1]).
-export([callback_mode/0]).
-export([dispatching/3]).
-export([listening/3]).
-export([listening/2]).
-export([terminate/3]).
-export([code_change/4]).
%%% ========================================================= %%%
-record(data, {regex=[], refs=[]}).
-define(POOL, erlcount).
%%% ========================================================= %%%
%%% ===================== API Functions ===================== %%%
%%% ========================================================= %%%
-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_statem:start_link(?MODULE, [], []).

-spec complete(pid(), string(), reference(), integer()) -> no_return().
complete(Pid, Regex, Ref, Count) ->
    gen_statem:cast(Pid, {complete, Regex, Ref, Count}).

%%% ========================================================= %%%
%%% =================== State Functions ===================== %%%
%%% ========================================================= %%%
dispatching(info, {start, Dir}, Data) ->
    gen_statem:cast(self(), erlcount_lib:find_erl(Dir)),
    {next_state, dispatching, Data};
dispatching(_EventType, {continue, File, Continuation},
            Data = #data{ regex=Re, refs=Refs }) ->
    F = fun({Regex, _Count}, NewRefs) ->
        Ref = make_ref(),
        ppool_app:async_queue(?POOL, [self(), Ref, File, Regex]),
        [Ref|NewRefs]
    end,
    NewRefs = lists:foldl(F, Refs, Re),
    gen_statem:cast(self(), Continuation()),
    {next_state, dispatching, Data#data{refs = NewRefs}};
dispatching(_EventType, {complete, Regex, Ref, Count},
            Data = #data{regex=Re, refs=Refs}) ->
    {Regex, OldCount} = lists:keyfind(Regex, 1, Re),
    NewRe = lists:keyreplace(Regex, 1, Re, {Regex, OldCount+Count}),
    NewData = Data#data{regex=NewRe, refs=Refs--[Ref]},
    {next_state, dispatching, NewData};
dispatching(_Event, done, #data{regex=Re, refs=[]}) ->
    [io:format("Regex ~s has ~p results~n", [R,C]) || {R, C} <- Re],
    {stop, normal, done};
dispatching(_Event, done, Data) ->
    {next_state, listening, Data}.

listening(_EventType, {complete, Regex, Ref, Count},
          Data = #data{regex=Re, refs=Refs}) ->
    {Regex, OldCount} = lists:keyfind(Regex, 1, Re),
    NewRe = lists:keyreplace(Regex, 1, Re, {Regex, OldCount+Count}),
    NewData = Data#data{regex=NewRe, refs=Refs--[Ref]},
    listening(done, NewData).

listening(done, #data{regex=Re, refs=[]}) -> % all received!
    [io:format("Regex ~s has ~p results~n", [R,C]) || {R, C} <- Re],
    {stop, normal, done};
listening(done, Data) -> % entries still missing
    {next_state, listening, Data}.

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init([]) ->
    {ok, Re} = application:get_env(regex),
    {ok, Dir} = application:get_env(directory),
    {ok, MaxFiles} = application:get_env(max_files),
    ppool_app:start_pool(?POOL, MaxFiles, {erlcount_counter, start_link, []}),
    case lists:all(fun valid_regex/1, Re) of
        true ->
            self() ! {start, Dir},
            {ok, dispatching, #data{ regex=[{R, 0} || R <- Re] }};
        false ->
            {stop, invalid_regex}
    end.

callback_mode() -> state_functions.

terminate(_Reason, _State, _Data) ->
        ok.

code_change(_OldVsn, State, Data, _Extra) ->
    {ok, State, Data}.

%%% ========================================================= %%%
%%% ================= Internal Functions ==================== %%%
%%% ========================================================= %%%
valid_regex(Re) ->
    try re:run("", Re) of
        _Result -> true
    catch
        error:badarg -> false
    end.
-module(ppool_worker_sup).
%%% ========================================================= %%%
-behaviour(supervisor).
%%% ========================================================= %%%
-export([start_link/1]).

-export([init/1]).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start_link(mfa()) -> {ok, pid()}.
start_link(MFA = {_Mod, _Func, _Args}) ->
    supervisor:start_link(?MODULE, MFA).

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init({Mod, Func, Args}) ->
    SupFlags = #{ strategy => simple_one_for_one,
                  intensity => 5,
                  period => 3600 },
    ChildSpecs = #{ id => ppool_worker,
                    start => {Mod, Func, Args},
                    restart => temporary,
                    shutdown => 5000, % Shutdown time
                    type => worker,
                    modules => [Mod] },
    {ok, {SupFlags, [ChildSpecs]}}.
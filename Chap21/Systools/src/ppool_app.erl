-module(ppool_app).
%%% ========================================================= %%%
-behaviour(application).
%%% ========================================================= %%%
-export([start/2]).
-export([stop/1]).

-export([start_pool/3]).
-export([stop_pool/1]).
-export([run/2]).

-export([sync_queue/2]).
-export([async_queue/2]).
%%% ========================================================= %%%
-type start_type() :: normal.
-type args() :: []. %% See ppool.app.src, {mod, {ppool_app, []}}.
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start(start_type(), args()) -> {ok, pid()}.
start(_StartType, _StartArgs) ->
    ppool_supersup:start_link().

-spec stop(term()) -> no_return().
stop(_State) ->
    ok.

-spec start_pool(atom(), integer(), mfa()) -> {ok, pid()}.
start_pool(Name, Limit, {M,F,A}) ->
    ppool_supersup:start_pool(Name, Limit, {M,F,A}).

-spec stop_pool(atom()) -> no_return().
stop_pool(Name) ->
    ppool_supersup:stop_pool(Name).

-spec run(atom(), term()) -> term().
run(Name, Args) ->
    ppool_serv:run(Name, Args).

-spec sync_queue(atom(), term()) -> term().
sync_queue(Name, Args) ->
    ppool_serv:sync_queue(Name, Args).

-spec async_queue(atom(), term()) -> no_return().
async_queue(Name, Args) ->
    ppool_serv:async_queue(Name, Args).

-module(ppool_sup).
%%% ========================================================= %%%
-behaviour(supervisor).
%%% ========================================================= %%%
-export([start_link/3]).

-export([init/1]).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start_link(atom(), integer(), mfa()) -> {ok, pid()}.
start_link(Name, Limit, MFA) ->
    supervisor:start_link(?MODULE, {Name, Limit, MFA}).

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init({Name, Limit, MFA}) ->
    SupFlags = #{ strategy => one_for_all,
                  intensity => 1,
                  period => 3600 },
    ChildSpecs = #{ id => serv,
                    start => {ppool_serv, start_link,
                              [Name, Limit, self(), MFA]},
                    restart => permanent,
                    shutdown => 5000, % Shutdown time
                    type => worker,
                    modules => [ppool_serv] },
    {ok, {SupFlags, [ChildSpecs]}}.
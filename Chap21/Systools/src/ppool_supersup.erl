-module(ppool_supersup).
%%% ========================================================= %%%
-behaviour(supervisor).
%%% ========================================================= %%%
-export([start_link/0]).
-export([stop/0]).

-export([start_pool/3]).
-export([stop_pool/1]).

-export([init/1]).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start_link() -> {ok, pid()}.
start_link() ->
    supervisor:start_link({local, ppool}, ?MODULE, []).

-spec stop() -> no_return().
stop() ->
    case whereis(ppool) of
        Pid when is_pid(Pid) ->
            exit(Pid, kill);
        _Other ->
            ok
    end.

-spec start_pool(atom(), integer(), mfa()) -> {ok, pid()}.
start_pool(Name, Limit, MFA) ->
    ChildSpec = #{ id => Name,
                   start => {ppool_sup, start_link,
                             [Name, Limit, MFA]},
                   restart => permanent,
                   shutdown => 10500,
                   type => supervisor,
                   modules => [ppool_sup] },
    supervisor:start_child(ppool, ChildSpec).

-spec stop_pool(atom()) -> no_return().
stop_pool(Name) ->
    supervisor:terminate_child(ppool, Name),
    supervisor:delete_child(ppool, Name).

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init([]) ->
    SupFlags = #{ strategy => one_for_one,
                  intensity => 6,
                  period => 3600 },
    {ok, {SupFlags, []}}.
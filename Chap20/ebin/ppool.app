{application, ppool,
 [{description, "An OTP application"},
  {vsn, "1.0.0"},
  {registered, [ppool]},
  {mod, {ppool_app, []}},
  {applications,
   [kernel,
    stdlib
   ]},
  {env,[]},
  {modules, [ppool_app, ppool_serv, ppool_sup,
             ppool_supersup, ppool_worker_sup]},

  {licenses, ["Apache-2.0"]},
  {links, []}
 ]}.

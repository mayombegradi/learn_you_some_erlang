-module(cat_fsm).
%%% ========================================================= %%%
-export([start/0]).
-export([event/2]).

-export([dont_give_crap/0]).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start() -> pid().
start() ->
    spawn(fun() -> dont_give_crap() end).

-spec event(pid(), atom()) -> term().
event(Pid, Event) ->
    Ref = make_ref(), % Won't care for monitors here
    Pid ! {self(), Ref, Event},
    receive
        {Ref, Msg} -> {ok, Msg}
    after 5000 ->
        {error, timeout}
    end.

%%% ========================================================= %%%
%%% =================== State Functions ===================== %%%
%%% ========================================================= %%%
dont_give_crap() ->
    receive
        {Pid, Ref, _Msg} -> Pid ! {Ref, meh};
        _Other -> ok
    end,
    io:format("Switching to 'don_give_crap' state~n"),
    dont_give_crap().
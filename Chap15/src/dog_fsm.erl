-module(dog_fsm).
%%% ========================================================= %%%
-export([start/0]).
-export([squirrel/1]).
-export([pet/1]).

-export([bark/0]).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start() -> pid().
start() ->
    spawn(fun() -> bark() end).

-spec squirrel(pid()) -> no_return().
squirrel(Pid) ->
    Pid ! pet.

-spec pet(pid()) -> no_return().
pet(Pid) -> Pid ! pet.

%%% ========================================================= %%%
%%% =================== State Functions ===================== %%%
%%% ========================================================= %%%
bark() ->
    io:format("Dog says: BARK! BARK!~n"),
    receive
        pet ->
            wag_tail();
        _Other ->
            io:format("Dog is confused~n"),
            bark()
    after 2000 ->
        bark()
    end.

wag_tail() ->
    io:format("Dog wags its tail~n"),
    receive
        pet ->
            sit();
        _Other ->
            io:format("Dog is confused~n"),
            wag_tail()
    after 30000 ->
        bark()
    end.

sit() ->
    io:format("Dog is sitting. Goooood boy!~n"),
    receive
        squirrel ->
            bark();
        _Other ->
            io:format("Dog is confused~n"),
            sit()
    end.
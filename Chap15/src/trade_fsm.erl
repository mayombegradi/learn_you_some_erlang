-module(trade_fsm).
%%% ========================================================= %%%
-behaviour(gen_statem).
%%% ========================================================= %%%
-export([start/1]).
-export([start_link/1]).

-export([trade/2]).
-export([accept_trade/1]).
-export([make_offer/2]).
-export([retract_offer/2]).
-export([ready/1]).
-export([cancel/1]).

-export([ask_negotiate/2]).
-export([accept_negotiate/2]).
-export([do_offer/2]).
-export([undo_offer/2]).
-export([are_you_ready/1]).
-export([not_yet/1]).
-export([am_ready/1]).
-export([ack_trans/1]).
-export([ask_commit/1]).
-export([do_commit/1]).
-export([notify_cancel/1]).

-export([init/1]).
-export([callback_mode/0]).
-export([terminate/3]).

-export([idle/3]).
-export([idle_wait/3]).
-export([negotiate/3]).
-export([wait/3]).
-export([ready/3]).

-export([handle_event/4]).

%%% ========================================================= %%%
-type opaque() :: term().
-type reply_tag() :: opaque().
-type from() :: {pid(), reply_tag()}.

-record(data, { name=""        :: string(),
                other          :: pid(),
                own_items=[]   :: list(),
                other_items=[] :: list(),
                monitor        :: reference(),
                from           :: from() }).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start(string()) -> {ok, pid()}.
start(Name) ->
    gen_statem:start(?MODULE, [Name], []).

-spec start_link(string()) -> {ok, pid()}.
start_link(Name) ->
    gen_statem:start_link(?MODULE, [Name], []).

%% Ask for a begin session. Returns when/if the other accepts.
-spec trade(pid(), pid()) -> term().
trade(OwnPid, OtherPid) ->
    gen_statem:call(OwnPid, {negotiate, OtherPid}, 30000).

%% Accept someone's trade offer.
-spec accept_trade(pid()) -> term().
accept_trade(OwnPid) ->
    gen_statem:call(OwnPid, accept_negotiate).

%% Send an item on the table to be traded.
-spec make_offer(pid(), term()) -> no_return().
make_offer(OwnPid, Item) ->
    gen_statem:cast(OwnPid, {make_offer, Item}).

%% Cancel trade offer.
-spec retract_offer(pid(), term()) -> no_return().
retract_offer(OwnPid, Item) ->
    gen_statem:cast(OwnPid, {retract_offer, Item}).

%% Mention that you're ready for a trade. When the other
%% player also declares they're ready, the trade is done.
-spec ready(pid()) -> term().
ready(OwnPid) ->
    gen_statem:call(OwnPid, ready, infinity).

%% Cancel the transaction.
%% Sync send all event
-spec cancel(pid()) -> term().
cancel(OwnPid) ->
    gen_statem:call(OwnPid, cancel).

%%% ========================================================= %%%
%%% ================= FSM to FSM Functions ================== %%%
%%% ========================================================= %%%
%% Ask the other FSM's Pid for a trade session.
-spec ask_negotiate(pid(), pid()) -> term().
ask_negotiate(OtherPid, OwnPid) ->
    gen_statem:cast(OtherPid, {ask_negotiate, OwnPid}).

%% Forward the client message accepting the transaction.
-spec accept_negotiate(pid(), pid()) -> no_return().
accept_negotiate(OtherPid, OwnPid) ->
    gen_statem:cast(OtherPid, {accept_negotiate, OwnPid}).

%% Forward a client's offer.
-spec do_offer(pid(), term()) -> term().
do_offer(OtherPid, Item) ->
    gen_statem:cast(OtherPid, {do_offer, Item}).

%% Forward a client's offer cancellation.
-spec undo_offer(pid(), term()) -> term().
undo_offer(OtherPid, Item) ->
    gen_statem:cast(OtherPid, {undo_offer, Item}).

%% Ask the other side if he's ready to trade.
-spec are_you_ready(pid()) -> term().
are_you_ready(OtherPid) ->
    gen_statem:cast(OtherPid, are_you_ready).

%% Reply that the side is not ready to trade,
%% i.e. is not in 'wait' state.
-spec not_yet(pid()) -> term().
not_yet(OtherPid) ->
    gen_statem:cast(OtherPid, not_yet).

%% Tells the other fsm that the user is currently waiting
%% for the ready state. State should transition to 'ready'.
-spec am_ready(pid()) -> term().
am_ready(OtherPid) ->
    gen_statem:cast(OtherPid, 'ready!').

%% Acknowledge that the fsm is in a ready state.
-spec ack_trans(pid()) -> term().
ack_trans(OtherPid) ->
    gen_statem:cast(OtherPid, ack).

%% Ask if ready to commit.
-spec ask_commit(pid()) -> term().
ask_commit(OtherPid) ->
    gen_statem:call(OtherPid, ask_commit).

%% Begin the synchronous commit.
-spec do_commit(pid()) -> term().
do_commit(OtherPid) ->
    gen_statem:call(OtherPid, do_commit).

%% Send all event!
-spec notify_cancel(pid()) -> term().
notify_cancel(OtherPid) ->
    gen_statem:call(OtherPid, cancel).
%%% ========================================================= %%%
%%% ================== Callback Functions =================== %%%
%%% ========================================================= %%%
init([Name]) ->
    process_flag(trap_exit, true),
    {ok, idle, #data{ name=Name }}.

callback_mode() -> state_functions.

%% Transaction completed.
terminate(normal, ready, Data=#data{}) ->
    notice(Data, "FSM leaving.", []);
    terminate(_Reason, _StateName, _StateData) ->
    ok.

%%% ========================================================= %%%
%%% =================== State Functions ===================== %%%
%%% ========================================================= %%%
idle(cast, {ask_negotiate, OtherPid}, Data=#data{}) ->
    Ref = monitor(process, OtherPid),
    notice(Data, "~p asked for a trade negotiation", [OtherPid]),
    NewData = Data#data{ other=OtherPid, monitor=Ref },
    {next_state, idle_wait, NewData};
idle({call, From}, {negotiate, OtherPid}, Data=#data{}) ->
    ask_negotiate(OtherPid, self()),
    notice(Data, "asking user ~p for a trade", [OtherPid]),
    Ref = monitor(process, OtherPid),
    NewData = Data#data{ other=OtherPid, monitor=Ref, from=From },
    {next_state, idle_wait, NewData};
idle(EventType, Event, Data) ->
    handle_event(EventType, Event, ?FUNCTION_NAME, Data).

idle_wait(cast, {ask_negotiate, OtherPid}, Data=#data{ other=OtherPid }) ->
    gen_statem:reply(Data#data.from, ok),
    notice(Data, "starting negotiation", []),
    {next_state, negotiate, Data};
%% The other side has accepted our offer. Move to negotiate state.
idle_wait(cast, {accept_negotiate, OtherPid}, Data=#data{ other=OtherPid }) ->
    gen_statem:reply(Data#data.from, ok),
    notice(Data, "starting negotiation", []),
    {next_state, negotiate, Data};
idle_wait({call, From}, accept_negotiate, Data=#data{ other=OtherPid }) ->
    accept_negotiate(OtherPid, self()),
    notice(Data, "accepting negotiation", []),
    gen_statem:reply(From, ok),
    {next_state, negotiate, Data};
idle_wait(EventType, Event, Data) ->
    handle_event(EventType, Event, ?FUNCTION_NAME, Data).

negotiate(cast, {make_offer, Item}, Data=#data{ own_items=OwnItems }) ->
    do_offer(Data#data.other, Item),
    notice(Data, "offering ~p", [Item]),
    NewData = Data#data{ own_items=add(Item, OwnItems) },
    {next_state, negotiate, NewData};
%% Own side retracting an item offer.
negotiate(cast, {retract_offer, Item}, Data=#data{ own_items=OwnItems }) ->
    undo_offer(Data#data.other, Item),
    notice(Data, "cancelling offer on ~p", [Item]),
    NewData = Data#data{ own_items=remove(Item, OwnItems) },
    {next_state, negotiate, NewData};
%% Other side offering an item.
negotiate(cast, {do_offer, Item}, Data=#data{ other_items=OtherItems }) ->
    notice(Data, "other player offering ~p", [Item]),
    NewData = Data#data{ other_items=add(Item, OtherItems) },
    {next_state, negotiate, NewData};
%% Other side retracting an item offer.
negotiate(cast, {undo_offer, Item}, Data=#data{ other_items=OtherItems }) ->
    notice(Data, "Other player cancelling offer on ~p", [Item]),
    NewData = Data#data{ other_items=remove(Item, OtherItems) },
    {next_state, negotiate, NewData};
negotiate(cast, are_you_ready, Data=#data{ other=OtherPid }) ->
    io:format("Other user ready to trade.~n"),
    Msg = "Other user ready to transfer goods:~n" ++
        "You get ~p, The other side gets ~p",
    notice(Data, Msg, [Data#data.other_items, Data#data.own_items]),
    not_yet(OtherPid),
    {next_state, negotiate, Data};
negotiate({call, From}, ready, Data=#data{ other=OtherPid }) ->
    are_you_ready(OtherPid),
    notice(Data, "asking if ready, waiting", []),
    {next_state, wait, Data#data{ from=From} };
negotiate(EventType, Event, Data) ->
    handle_event(EventType, Event, ?FUNCTION_NAME, Data).

wait(cast, {do_offer, Item}, Data=#data{ other_items=OtherItems }) ->
    gen_statem:reply(Data#data.from, offer_changed),
    notice(Data, "other side offering ~p", [Item]),
    {next_state, negotiate, Data#data{other_items=add(Item, OtherItems)}};
wait(cast, {undo_offer, Item}, Data=#data{other_items=OtherItems}) ->
    gen_statem:reply(Data#data.from, offer_changed),
    notice(Data, "Other side cancelling offer of ~p", [Item]),
    {next_state, negotiate, Data#data{other_items=remove(Item, OtherItems)}};
wait(cast, are_you_ready, Data=#data{}) ->
    am_ready(Data#data.other),
    notice(Data, "asked if ready, and I am. Waiting for same reply", []),
    {next_state, wait, Data};
wait(cast, not_yet, Data=#data{}) ->
    notice(Data, "Other not ready yet", []),
    {next_state, wait, Data};
wait(cast, 'ready!', Data=#data{}) ->
    am_ready(Data#data.other),
    ack_trans(Data#data.other),
    gen_statem:reply(Data#data.from, ok),
    notice(Data, "other side is ready. Moving to ready state", []),
    {next_state, ready, Data};
%% Don't care about these!
wait(EventType, Event, Data) ->
    handle_event(EventType, Event, ?FUNCTION_NAME, Data).


ready(cast, ack, Data=#data{}) ->
    case priority(self(), Data#data.other) of
        true ->
            try
                notice(Data, "asking for commit", []),
                ready_commit = ask_commit(Data#data.other),
                notice(Data, "ordering commit", []),
                ok = do_commit(Data#data.other),
                notice(Data, "committing...", []),
                commit(Data),
                {stop, normal, Data}
            catch Class:Reason ->
            %% Abort! Either ready_commit or do_commit failed.
                notice(Data, "commit failed", []),
                {stop, {Class, Reason}, Data}
            end;
        false ->
        {next_state, ready, Data}
    end;
ready({call, From}, ask_commit, Data) ->
    notice(Data, "replying to ask_commit", []),
    gen_statem:reply(From, ready_commit),
    {next_state, ready, Data};
ready({call, From}, do_commit, Data) ->
    notice(Data, "committing...", []),
    commit(Data),
    gen_statem:reply(From, ok),
    {stop, normal, Data};
ready(EventType, Event, Data) ->
    handle_event(EventType, Event, ?FUNCTION_NAME, Data).

%% The other player has sent this cancel event.
%% Stop whatever we're doing and shut down!
handle_event(cast, cancel, _StateName, Data=#data{}) ->
    notice(Data, "received cancel event", []),
    {stop, other_cancelled, Data};
handle_event(cast, Event, StateName, Data) ->
    unexpected(Event, StateName),
    {next_state, StateName, Data};
%% This cancel event comes from the client. We must warn the other
%% player that we have a quitter!
handle_event({call, _From}, cancel, _StateName, Data=#data{}) ->
    notify_cancel(Data#data.other),
    notice(Data, "cancelling trade, sending cancel event", []),
    {stop, cancelled, ok, Data};
%% Note: DO NOT reply to unexpected calls. Let the call-maker crash!
handle_event({call, _From}, Event, StateName, Data) ->
    unexpected(Event, StateName),
    {next_state, StateName, Data};
handle_event(_EventType, {'DOWN', Ref, process, Pid, Reason},
        _StateName, Data=#data{ other=Pid, monitor=Ref }) ->
    notice(Data, "Other side dead", []),
    {stop, {other_down, Reason}, Data};
handle_event(_EventType, Info, StateName, Data) ->
    unexpected(Info, StateName),
    {next_state, StateName, Data}.

%%% ========================================================= %%%
%%% =================== Helper Functions ==================== %%%
%%% ========================================================= %%%
%% Send players a notice. This could be messages to their clients
%% but for our purposes, outputting to the shell is enough.
notice(#data{ name=Name }, Info, Args) ->
    io:format("~s: "++Info++"~n", [Name|Args]).

%% Allows to log unexpected messages.
unexpected(Event, Data) ->
    Info = "~p received unknown event ~p while in state ~p~n",
    io:format(Info, [self(), Event, Data]).

%% Adds an item to an item list.
add(Item, Items) ->
    [Item | Items].

%% Removes an item from an item list.
remove(Item, Items) ->
    Items -- [Item].

priority(OwnPid, OtherPid) when OwnPid > OtherPid -> true;
priority(OwnPid, OtherPid) when OwnPid < OtherPid -> false.

commit(Data=#data{}) ->
    io:format("Transaction completed for ~s. "
    "Items sent are:~n~p,~n received are:~n~p.~n"
    "This operation should have some atomic save "
    "in a database.~n",
    [Data#data.name, Data#data.own_items, Data#data.other_items]).
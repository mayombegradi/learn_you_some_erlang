-module(band_supervisor).
%%% ========================================================= %%%
-behaviour(supervisor).
%%% ========================================================= %%%
-export([start_link/1]).

-export([init/1]).
%%% ========================================================= %%%
-type type_arg() :: lenient | angry | jerk.
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start_link(type_arg()) -> {ok, pid()}.
start_link(Type) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, Type).

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
%% The band supervisor will allow its band members to make a few
%% mistakes before shutting down all operations, based on what
%% mood he's in. A lenient supervisor will tolerate more mistakes
%% than an angry supervisor, who'll tolerate more than a
%% complete jerk supervisor.
init(lenient) ->
    init(#{ strategy => one_for_one,
            intensity => 3,
            period => 60 });
init(angry) ->
    init(#{ strategy => rest_for_one,
            intensity => 2,
            period => 60 });
init(jerk) ->
    init(#{ strategy => one_for_all,
            intensity => 1,
            period => 60 });
init(jamband) ->
    SupFlag = #{ strategy => simple_one_for_one,
                 intensity => 3,
                 period => 60 },
    {ok, {SupFlag, [child_spec()]}};
init(SupFlag=#{}) ->
    {ok, {SupFlag, children_specs()}}.

%%% ========================================================= %%%
%%% ================= Internal Functions ==================== %%%
%%% ========================================================= %%%
child_spec() ->
    #{ id => jam_musician,
       start => {musicians, start_link, []},
       restart => temporary,
       shutdown => 1000,
       type => worker,
       modules => [musicians] }.

children_specs() ->
    [
     #{ id => singer,
        start => {musicians, start_link, [singer, good]},
        restart => permanent,
        shutdown => 1000,
        type => worker,
        modules => [musicians] },
     #{ id => bass,
        start => {musicians, start_link, [bass, good]},
        restart => temporary,
        shutdown => 1000,
        type => worker,
        modules => [musicians] },
     #{ id => drum,
        start => {musicians, start_link, [drum, bad]},
        restart => transient,
        shutdown => 1000,
        type => worker,
        modules => [musicians] },
     #{ id => keytar,
        start => {musicians, start_link, [keytar, good]},
        restart => transient,
        shutdown => 1000,
        type => worker,
        modules => [musicians] }
    ].

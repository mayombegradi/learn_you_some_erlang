-module(musicians).
%%% ========================================================= %%%
-behaviour(gen_server).
%%% ========================================================= %%%
-export([start_link/2]).
-export([stop/1]).

-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
%%% ========================================================= %%%
-type role() :: string() | atom().
-type skill() :: string() | atom().
%%% ========================================================= %%%
-record(state, { name="",
                 role :: role(),
                 skill :: skill() }).
%%% ========================================================= %%%
-define(DELAY, 750).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start_link(role(), skill()) -> {ok, pid()}.
start_link(Role, Skill) ->
    gen_server:start_link({local, Role}, ?MODULE, [Role, Skill], []).

-spec stop(role()) -> no_return().
stop(Role) -> gen_server:call(Role, stop).
%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init([Role, Skill]) ->
    %% To know when the parent shuts down.
    process_flag(trap_exit, true),
    %% Sets a seed for random number generation for the life of the process.
    %% Uses the current time to do it. Unique value guaranteed by now().
    rand:seed(exsss),
    TimeToPlay = rand:uniform(3000),
    Name = pick_name(),
    StrRole = atom_to_list(Role),
    Msg = "Musician ~s, playing the ~s entered the room~n",
    io:format(Msg, [Name, StrRole]),
    {ok, #state{ name=Name, role=StrRole, skill=Skill}, TimeToPlay }.

handle_call(stop, _From, State=#state{}) ->
    {stop, normal, ok, State};
handle_call(_Message, _From, State) ->
    {noreply, State, ?DELAY}.

handle_cast(_Message, State) ->
    {noreply, State, ?DELAY}.

handle_info(timeout, State=#state{ name=Name, skill=good }) ->
    io:format("~s produced sound!~n",[Name]),
    {noreply, State, ?DELAY};
handle_info(timeout, State=#state{ name=Name, skill=bad }) ->
    case rand:uniform(5) of
        1 ->
            io:format("~s played a false note. Uh oh~n",[Name]),
            {stop, bad_note, State};
        _Int ->
            io:format("~s produced sound!~n",[Name]),
            {noreply, State, ?DELAY}
    end;
handle_info(_Message, State) ->
    {noreply, State, ?DELAY}.

terminate(normal, State) ->
    Msg = "~s left the room (~s)~n",
    io:format(Msg, [State#state.name, State#state.role]);
terminate(bad_note, State) ->
    Msg = "~s sucks! kicked that member out of the band! (~s)~n",
    io:format(Msg, [State#state.name, State#state.role]);
terminate(shutdown, State) ->
    Msg = "The manager is mad and fired the whole band! " ++
          "~s just got back to playing in the subway~n",
    io:format(Msg, [State#state.name]);
terminate(_Reason, State) ->
    Msg = "~s has been kicked out (~s)~n",
    io:format(Msg, [State#state.name, State#state.role]).
%%% ========================================================= %%%
%%% ================= Internal Functions ==================== %%%
%%% ========================================================= %%%
%% Yes, the names are based off the magic school bus characters'
%% 10 names!
pick_name() ->
    %% The seed must be set for the random functions. Use within the
    %% process that started with init/1.
    lists:nth(rand:uniform(10), firstnames()) ++ " " ++
    lists:nth(rand:uniform(10), lastnames()).

firstnames() ->
    ["Valerie", "Arnold", "Carlos", "Dorothy", "Keesha",
     "Phoebe", "Ralphie", "Tim", "Wanda", "Janet"].

lastnames() ->
    ["Frizzle", "Perlstein", "Ramon", "Ann", "Franklin",
     "Terese", "Tennelli", "Jamal", "Li", "Perlstein"].
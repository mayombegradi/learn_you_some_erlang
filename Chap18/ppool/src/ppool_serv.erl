-module(ppool_serv).
%%% ========================================================= %%%
-behaviour(gen_server).
%%% ========================================================= %%%
-export([start/4]).
-export([start_link/4]).
-export([stop/1]).
-export([run/2]).
-export([sync_queue/2]).
-export([async_queue/2]).

-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
%%% ========================================================= %%%
-record(state, { limit=0,
                 sup :: pid(),
                 queue=queue:new(),
                 refs=gb_sets:empty() }).
%%% ========================================================= %%%
-type sup() :: pid().
-type name() :: atom().
-type args() :: term().
-type limit() :: integer().
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
-spec start(name(), limit(), sup(), mfa()) -> {ok, pid()}.
start(Name, Limit, Sup, MFA) when is_atom(Name), is_integer(Limit) ->
    gen_server:start({local, Name}, ?MODULE, {Limit, MFA, Sup}, []).

-spec start_link(name(), limit(), sup(), mfa()) -> {ok, pid()}.
start_link(Name, Limit, Sup, MFA) when is_atom(Name), is_integer(Limit) ->
    gen_server:start_link({local, Name}, ?MODULE, {Limit, MFA, Sup}, []).

-spec stop(name()) -> no_return().
stop(Name) ->
    gen_server:stop(Name).

-spec run(name(), args()) -> term().
run(Name, Args) ->
    gen_server:call(Name, {run, Args}).

-spec sync_queue(name(), args()) -> term().
sync_queue(Name, Args) ->
    gen_server:call(Name, {sync, Args}, infinity).

-spec async_queue(name(), args()) -> no_return().
async_queue(Name, Args) ->
    gen_server:cast(Name, {async, Args}).

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init({Limit, MFA, Sup}) ->
    self() ! {start_worker_supervisor, Sup, MFA},
    {ok, #state{ limit=Limit }}.

handle_call({run, Args}, _From, State=#state{ limit=N, sup=Sup, refs=R }) when N > 0 ->
    {ok, Pid} = supervisor:start_child(Sup, Args),
    Ref = erlang:monitor(process, Pid),
    {reply, {ok, Pid}, State#state{ limit=N-1, refs=gb_sets:add(Ref, R) }};
handle_call({run, _Args}, _From, State=#state{ limit=N }) when N =< 0 ->
    {reply, noalloca, State};
handle_call({sync, Args}, _From, State=#state{ limit=N, sup=Sup, refs=R }) when N > 0 ->
    {ok, Pid} = supervisor:start_child(Sup, Args),
    Ref = erlang:monitor(process, Pid),
    {reply, {ok, Pid}, State#state{ limit=N-1, refs=gb_sets:add(Ref,R) }};
handle_call({sync, Args}, From, State=#state{queue=Q}) ->
    {noreply, State#state{ queue=queue:in({From, Args}, Q) }};
handle_call(_Msg, _From, State) ->
    {noreply, State}.

handle_cast({async, Args}, State=#state{ limit=N, sup=Sup, refs=R }) when N > 0 ->
    {ok, Pid} = supervisor:start_child(Sup, Args),
    Ref = erlang:monitor(process, Pid),
    {noreply, State#state{ limit=N-1, refs=gb_sets:add(Ref,R) }};
handle_cast({async, Args}, State=#state{ limit=N, queue=Q }) when N =< 0 ->
    {noreply, State#state{ queue=queue:in(Args,Q) }};
    %% Not going to explain the one below!
handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info({'DOWN', Ref, process, _Pid, _Reason}, State=#state{ refs=Refs }) ->
    io:format("received down msg~n"),
    case gb_sets:is_element(Ref, Refs) of
        true ->
            handle_down_worker(Ref, State);
        false ->
            {noreply, State} %% not our responsibility
    end;
handle_info({start_worker_supervisor, Sup, MFA}, State=#state{}) ->
    {ok, Pid} = supervisor:start_child(Sup, worker_sup_spec(MFA)),
    {noreply, State#state{ sup=Pid }};
handle_info(Msg, State) ->
    io:format("Unknown msg: ~p~n", [Msg]),
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

%%% ========================================================= %%%
%%% ================= Internal Functions ==================== %%%
%%% ========================================================= %%%
worker_sup_spec(MFA) ->
    #{ id => worker_sup,
       start => {ppool_worker_sup, start_link, [MFA]},
       restart => permanent,
       shutdown => 10000,
       type => supervisor,
       modules => [ppool_worker_sup] }.

handle_down_worker(Ref, State=#state{ limit=L, sup=Sup, refs=Refs }) ->
    case queue:out(State#state.queue) of
        {{value, {From, Args}}, Q} ->
            {ok, Pid} = supervisor:start_child(Sup, Args),
            NewRef = erlang:monitor(process, Pid),
            NewRefs = gb_sets:insert(NewRef, gb_sets:delete(Ref,Refs)),
            gen_server:reply(From, {ok, Pid}),
            {noreply, State#state{ refs=NewRefs, queue=Q }};
        {{value, Args}, Q} ->
            {ok, Pid} = supervisor:start_child(Sup, Args),
            NewRef = erlang:monitor(process, Pid),
            NewRefs = gb_sets:insert(NewRef, gb_sets:delete(Ref,Refs)),
            {noreply, State#state{ refs=NewRefs, queue=Q }};
        {empty, _Q} ->
            {noreply, State#state{ limit=L+1, refs=gb_sets:delete(Ref,Refs) }}
    end.
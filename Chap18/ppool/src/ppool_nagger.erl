-module(ppool_nagger).
%%% ========================================================= %%%
-behaviour(gen_server).
%%% ========================================================= %%%
-export([start_link/4]).
-export([stop/1]).

-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).

%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
start_link(Task, Delay, Max, SendTo) ->
    gen_server:start_link(?MODULE, {Task, Delay, Max, SendTo} , []).

stop(Pid) ->
    gen_server:call(Pid,stop).

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init({Task, Delay, Max, SendTo}) ->
    {ok, {Task, Delay, Max, SendTo}, Delay}.

%%% OTP Callbacks
handle_call(stop, _From, State) ->
    {stop, normal, ok, State};
handle_call(_Msg, _From, State) ->
    {noreply, State}.

handle_cast(_Msg, State) ->
{noreply, State}.

handle_info(timeout, {Task, Delay, Max, SendTo}) ->
    SendTo ! {self(), Task},
    if 
       Max =:= infinity -> {noreply, {Task, Delay, Max, SendTo}, Delay};
       Max =< 1 -> {stop, normal, {Task, Delay, 0, SendTo}};
       Max > 1 -> {noreply, {Task, Delay, Max-1, SendTo}, Delay}
    end.

%% We cannot use handle_info below: if that ever happens,
%% we cancel the timeouts (Delay) and basically zombify
%% the entire process. It's better to crash in this case.
%% handle_info(_Msg, State) ->
%% {noreply, State}.

terminate(_Reason, _State) -> ok.
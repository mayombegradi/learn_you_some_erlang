-module(kitty_gen_server).
%%% ========================================================= %%%
-behaviour(gen_server).
%%% ========================================================= %%%
-export([start_link/0]).
-export([order_cat/3]).
-export([return_cat/1]).
-export([close_shop/0]).

-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
%%% ========================================================= %%%
-record(cat, { name :: string(),
               color :: atom(),
               description :: string() }).
%%% ========================================================= %%%
%%% =================== API Functions ======================= %%%
%%% ========================================================= %%%
%%% ========================================================= %%%
-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

-spec order_cat(string(), atom(), string()) -> #cat{}.
order_cat(Name, Color, Description) ->
    gen_server:call(?MODULE, {order, Name, Color, Description}).

-spec return_cat(#cat{}) -> no_return().
return_cat(Cat) ->
    gen_server:cast(?MODULE, {return, Cat}).

-spec close_shop() -> no_return().
close_shop() ->
    gen_server:call(?MODULE, terminate).

%%% ========================================================= %%%
%%% ================= Callback Functions ==================== %%%
%%% ========================================================= %%%
init(_Args) ->
    process_flag(trap_exit, true),
    {ok, []}.

handle_call({order, Name, Color, Description}, _From, Cats) ->
    if Cats =:= [] ->
        {reply, make_cat(Name, Color, Description), Cats};
       Cats =/= [] ->
        {reply, hd(Cats), tl(Cats)}
    end;
handle_call(terminate, _From, Cats) ->
    {stop, normal, ok, Cats}.

handle_cast({return, Cat = #cat{}}, Cats) ->
    {noreply, [Cat|Cats]}.

handle_info(Msg, Cats) ->
    io:format("Unexpected message: ~p~n",[Msg]),
    {noreply, Cats}.

terminate(normal, Cats) ->
    [io:format("~p was set free.~n",[C#cat.name]) || C <- Cats],
    ok.

%%% ========================================================= %%%
%%% ================= Internal Functions ==================== %%%
%%% ========================================================= %%%
make_cat(Name, Col, Desc) ->
    #cat{name=Name, color=Col, description=Desc}.